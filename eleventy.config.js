const moment = require("moment");
const pluginRss = require("@11ty/eleventy-plugin-rss");
const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");

module.exports = function(eleventyConfig) {

  eleventyConfig.addPlugin(pluginRss);
  eleventyConfig.addPlugin(syntaxHighlight);
  eleventyConfig.addPassthroughCopy("img");

  eleventyConfig.addFilter("readableDate", (dateObj, format) => {
    return moment(dateObj).format(format);
  });

  return {
    dir: {
      input: "src",
      output: "public",
    }
  }
}
