// entrypoint for some (hopefully) useful scripts

import { readdir } from "node:fs/promises";
import { join } from "node:path";

const POSTS_DIR_PATH = "/home/rory/dev/sawyer.dev/src/posts/";

async function main() {
  let posts = await readdir(POSTS_DIR_PATH).then((files) =>
    files.filter((f) => f.endsWith(".md"))
      .map((f) => join(POSTS_DIR_PATH, f))
  );
  console.log(posts);
}

main();
