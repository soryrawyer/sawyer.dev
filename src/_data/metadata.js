module.exports = {
  "blogUrl": process.env.URL || "https://sawyer.dev/posts",
  "author": "rory sawyer",
  "url": "https://sawyer.dev"
};
