---
title: "Decoding MP3s, Volume 3: I Think I've Got What I Need"
date: 2018-04-25T10:10:52-04:00
tags: post
---

I've decided to abandon my project to write an MP3 decoder. This is the most significant project that I've officially declared myself "done" with at RC, and as such I feel I need to justify the decision to myself. By "most significant" I really mean "spent more than a couple hours on", but while at RC it seems important to have a clear sense of what it takes for me to consider myself being "done" with something. At the risk of overgeneralizing, I think for a lot of people the most important question that needs an answer is "did I learn something?". A finished product is necessary for showing, but it isn't necessary for learning, and part of what makes RC feel so refreshing (to me, at least) is that it's a rare place where learning is preferred to showing. It's a brief respite from the pressure to exchange a fruit of labor for money.

So I feel fine and not at all uncomfortable with my decision to kill MP-3PO. My favorite name for a project is now wasted on something that will almost certainly never see the light of day.

One obvious reason I'm deciding to abandon the project is that I couldn't get the dang thing to work. The specification is vague (and costs money) and leaves open the (quite realized) potential for different encoders to behave differently. To test my code, I used a combination of [MediaConch](https://mediaarea.net/MediaConch) and [other](https://github.com/hajimehoshi/go-mp3) [mp3](https://github.com/FlorisCreyf/mp3-decoder) [decoders](http://blog.bjrn.se/2008/10/lets-build-mp3-decoder.html), and while they were indispensable resources in learning how the MP3 codec works, I kept getting different information about the files I was trying to decode. I would get different header values, side information values, and even information on where frames started and ended wouldn't align.

Another source of frustration was the source code I was reading. I know enough of Golang to understand that example, but I don't know C++ or Haskell, the other two languages I saw examples for. Then, in digging into the C and assembly that comprises mpg123, I started to understand why so many libraries in higher-level languages rely on these libraries. MP3 decoding is a messy, complicated, and pretty thoroughly solved affair.

Another thing I learned while researching audio codecs is that each popular music service uses something different, and none of them use MP3. Spotify uses Ogg Vorbis (my new favorite thing to say), except in web browsers where it uses AAC. Apple uses m4p, a DRM-laden version of m4a, which is based on AAC. Bandcamp will let you download a bunch of different formats.

Some of these formats have native decoders in certain languages, some don't. But you know what will decode them? Some C library written by a team of people over the course of decades. Not some idiot who wanted to work on it because he was amused by the name he gave his project.

And so the more I learned, the more a certain point was driven home: these implementations aren't just good, they're good enough.

I feel confident that I'm ending my MP3 adventure at the right point. I just wanted a better way to store and play my music, and so I'm turning my attention back to [monty](https://github.com/SoryRawyer/monty).
