---
title: "First Steps with OCaml"
date: 2023-06-11T11:26:10-04:00
tags: post
---

After years of hemming and hawing, I have decided to learn OCaml. This post is a quick summary of two things: how I landed on OCaml and what my very first steps have been. The former topic is going to be a vibes-first explanation of my past experience and why I chose OCaml. The latter section is going to be a relatively straightforward account of how I've set up my environment so I can learn OCaml in a way that feels familiar to me.

## how I settled on OCaml

The "why a new language" part: I like learning new programming languages, and I've wanted to dive into ML languages for years. My only exposure to this group of languages has been a few school assignments and the first two or three chapters of many programming books. The takeaways from these experiences have been understanding basic syntax and configuring my text editor. There's a level of growth that I think just doesn't come without day-to-day experience, and that's what I'm after.

I've spent years going over the introductory material for Idris, Haskell, and OCaml. Each language has its own specific appeal: I want to write Idris and use dependent types; I want to write Haskell to make up for the Ph.D. I never pursued; I want to write OCaml because there are good reasons I never pursued that Ph.D. I had to use Standard ML for a class once, but as a Philadelphian from the Pennsylvania side I must reject New Jersey.

Ultimately, I picked OCaml. If there is any one thing that swayed me to this, it's a line from the end of this [post about OCaml](https://borretti.me/article/two-years-ocaml):

> Particularly if you what you want is “garbage collected Rust that’s not Go”, OCaml is a good choice.

I like Rust, I don't really like Go. I like Python, and I'm kind of hoping OCaml will feel to me like typed, compiled Python.

## my actual first steps with the language and its tooling

Eventually, I'll use OCaml for a project I would normally use Python for: parsing CSVs and printing some summary statistics about the data therein. This is a "first steps" post though. If using `utop` to get some experience with syntax is crawling, my project is probably closer to walking. I'm just trying to stand on my own right now.

[Real World OCaml](https://dev.realworldocaml.org/index.html), the seemingly-standard guide for new OCaml programmers, has been extremely helpful, though I've found it a better fit as a reference rather than a book through which one works linearly. Similarly, this [Cornell class](https://cs3110.github.io/textbook/cover.html) seems to be a pretty standard introduction to the language. It seems to cover similar ground as Real World OCaml, but it's always nice to have things worded and presented a little differently.

I like looking to [Exercism](https://exercism.org/tracks/ocaml) for practical examples, and this time was no exception. Exercism gave me enough of an example of [`OUnit`](https://ocaml.org/p/ounit2/latest) to let me write some tests. `dune`, a (the?) build system for OCaml, seems to fill the role that `cargo` has in Rust. With dune's command to set up a new project and Exercism's tests as an example, I started working on the [exercises](https://ocaml.org/problems) referenced on the OCaml homepage. With these resources, I've been able to get into the write-compile-run (or just write-test) loop with which I'm familiar.

## conclusion

That's it. I still can't take more than a few steps at a time before stumbling, but I can get to walking from here.
