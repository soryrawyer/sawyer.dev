---
title: "Listening to Music"
date: 2020-05-20T16:42:27-04:00
tags: post
---

Two years ago, I set a three-month goal for myself:

> Coming into the Recurse Center, I thought I’d found a solid project: move my music and videos into cloud storage and create a media player that can move stuff to and from local disk without the user knowing or caring

Two years later, I made some progress.

# what and/or why

A couple years ago, I had an iPhone and a Macbook and I used iTunes for pretty much all of my "owned" music. I could use iTunes to move files from my computer to my phone. It was nice and simple and mimicked how I would manage my music in high school, moving files from my computer to my iPod. This was after Spotify became the default music experience for many, and, I think, around the time Apple Music was becoming a Thing.

One day, as I was poking around the iTunes settings on my phone, I did something that replaced all the music on my phone with Apple Music versions of all the tracks. It did so sloppily, and so live versions, alternative versions, demo versions, they were all replaced with whatever Apple Music had. Needless to say, I was pretty bummed. And so, years later, I'm finally trying to make amends with my old music library.

ok, now that we've got some context, let's define some requirements! I would like something that can:

- store files remotely and make them accessible across devices (i.e. - a computer and a phone)
- allow me to browse the files in a somewhat user-friendly way
- cache files locally for offline use
- does some kind of metadata fill-in/standardization

# "comb"

Comb (working title) is my latest attempt at this project. Here are the components as of this post:

![outline of how the cli and webapp work](/img/comb-overview-simple.png)

There are two main components: a command-line tool and a web application.

## command-line tool

The CLI currently has only one job to do: ingest. This program accepts a file path as input. If the path is a directory, the directory is recursively traversed. For each audio file found, comb will:

- read some of the metadata (ID3 tags, vorbis comments, etc)
- try to standardize artist, release, and track names, among other things, by using the [Discogs API](https://www.discogs.com/developers)
- write the metadata to a database (postgres, in this case)
- write the file to Google Cloud Storage (GCS)

## web app

The web application is also fairly simple. There is a server with some basic endpoints like `/artists` and `/releases/<int:release_id>`.

The frontend lists available artists, releases, and tracks, and when you double click on a track name, the audio player at the top will load that song. That last bit of functionality is handled using [signed GCS URLs](https://cloud.google.com/storage/docs/access-control/signed-urls). These, in combination with [GCS CORS](https://cloud.google.com/storage/docs/configuring-cors) configuration, allow our frontend to stream audio files from GCS without requiring the frontend to have any kind of Google Cloud credentials.

Since this application lets users download as much data as they want from a GCS bucket I'm being charged for, I use [flask-login](https://flask-login.readthedocs.io/en/latest/) for authentication.

## deployment

Aside from the functionality listed above, a goal of this project is to learn. As such, I've decided to try and get more experience with tools and technologies I seldom use, either for work or otherwise. This includes breaking down the duopoly of AWS and GCP (sorry Azure) by running my services on Digital Ocean. Instead of Github or Gitlab, I chose to store my code on [Sourcehut](https://sourcehut.org/). I wrote some HTML, CSS, and Javascript for the first time since 2015.

I also used [Let's Encrypt](https://letsencrypt.org/) for the first time (via [Certbot](https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx)), and it was incredibly easy to use.

However, there was only so much new territory I could cover. The server and CLI are written in Python, the server uses Flask and runs under gunicorn, and is "deployed" via rsync.

Also, I did a lot of this work while on vacation, and responsible devops just feels too much like work. Eventually I'll get my web app running under either systemd or supervisor, but right now it's running because I ssh'd into the server, ran `./run.sh &`, and logged out. I don't know what else might be on the horizon, infrastructure-wise.

# the future

There are a number of programming areas that this project could provide a way into: Electron, web assembly, running "on prem" instead of "in the cloud", but I think my next step will be to learn some Kotlin and try putting together an Android application. After all, the whole point of this is to be able to move files from my computer to my phone.

I also hope this project will help me break out of the Spotify haze that my music listening has fallen into. While working on this project, I felt a connection to the music I chose to listen to that I hadn't felt in years. I could feel my engagement shift from passive consumption to active listening. As I continue to work on Comb, I hope I get to understand that feeling a little better and strengthen my connection to the artists whose work I enjoy so often.

code: [https://gitlab.com/soryrawyer/comb](https://gitlab.com/soryrawyer/comb)
