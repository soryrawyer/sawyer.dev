---
title: "Book Review: Zero to Production in Rust"
date: 2024-11-08T10:00:00-04:00
tags: draft-post
---

goal: try to keep notes on what about this book has worked well for me
- the testing migrations thing helped me port that to Tauri, where I'd need the binary itself to be able to run migrations because, in theory, it's a desktop application that I have no remote control over (unlike a production database that I manage and can connect to)
- demonstrating useful tools and explaining how things work:
  - cargo-expand: really helpful for demystifying async code
  - sqlx: goes into detail about _how_ sqlx provides type safety (by actually connection to the database at compile time)
